import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom';

import App1 from './sub-apps/app1/src/App'
import App2 from './sub-apps/app2/src/App'

function App() {
  return (
    <div className="App">
      <Router>
        <nav>
          <ul>
            <li>
              <Link to="/app1">APP1</Link>
              <ul>
                <li><Link to="/app1/page1">page1</Link></li>
                <li><Link to="/app1/page2">page2</Link></li>
                <li><Link to="/app1/page3">page3</Link></li>
              </ul>
            </li>
            <li>
              <Link to="/app2">APP2</Link>
              <ul>
                <li><Link to="/app2/page1">page1</Link></li>
                <li><Link to="/app2/page2">page2</Link></li>
                <li><Link to="/app2/page3">page3</Link></li>
              </ul>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route path="/app1">
            <App1 />
          </Route>
          <Route path="/app2">
           <App2 />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
